/**
 * @file messages.h
 * @brief Declaration of the different Message classes.
 * @author Alejandro Suarez
 * @version 0.1
 * @date 2016-11-07
 */

#ifndef ICAPS_SERVER_MESSAGES_H
#define ICAPS_SERVER_MESSAGES_H

#include <vector>
#include <tinyxml2.h>
#include <boost/shared_ptr.hpp>
#include <string>
#include <ostream>

namespace icaps_server
{

class Parser;


/** 
 * @brief Generic Message interface. All the Messages implement this interface.
 */
class Message
{
  public:

    typedef boost::shared_ptr<Message> Ptr;
    typedef boost::shared_ptr<const Message> ConstPtr;

    /** 
     * @brief Tells whether this is a Server-side message or a Client-side
     * message.
     * 
     * @return true iff this is a Server-side message
     */
    virtual bool server() const =0;


    /** 
     * @brief Gives the type of Message (session-request, round-request, ...)
     * 
     * @return String with the name of the Message's type.
     */
    virtual const char *type() const =0;


    /** 
     * @brief Prints a string representation of this Message to a given
     * output stream
     * 
     * @param os Output stream.
     */
    virtual void print(std::ostream &os) const =0;


};

/////////////////////
// SERVER MESSAGES //
/////////////////////

/** 
 * @brief Abstract class that represents a Message from the server side.
 */
class ServerMessage : public Message
{
  private:

    std::string xml_tmp_;

  public:

    typedef boost::shared_ptr<ServerMessage> Ptr;
    typedef boost::shared_ptr<const ServerMessage> ConstPtr;

    /** 
     * @brief Constant return value.
     * 
     * @return true
     */
    inline virtual bool server() const
    { return true; }

    /** 
     * @brief Obtains XML representation of this message.
     * 
     * @param xml string where the XML representation shall be stored.
     */
    virtual void get_xml(std::string &xml) const =0;

    /**
     * Convenience method. Just calls get_xml(std::string&) passing an
     * inner reference and returns a constant pointer to that reference. That
     * the caller does not have to allocate the string himself :-)
     * @return XML string representation
     */
    const std::string& get_xml();

    virtual void print(std::ostream &os) const =0;

};


/**
 *
 */
class SessionInitMsg : public ServerMessage
{
  private:

    std::string session_id_;
    int n_rounds_;
    float time_allowed_;

  public:

    typedef boost::shared_ptr<SessionInitMsg> Ptr;
    typedef boost::shared_ptr<const SessionInitMsg> ConstPtr;

    /**
     * Constructor and initializator
     * @param id Identifier of this session
     * @param n_rounds Number of rounds of this session
     * @param time_allowed Time allowed for this session
     * @return
     */
    SessionInitMsg(const std::string &id="", int n_rounds=0, float time_allowed=9E6);

    // Just a bunch of getters and setters. Their usage should be self-explanatory,
    // so they are left undocumented.

    inline const std::string &id() const
    { return session_id_; }

    inline void id(const std::string &id)
    { session_id_ = id; }

    inline int n_rounds() const
    { return n_rounds_; }

    inline void n_rounds(int n_rounds)
    { n_rounds_ = n_rounds; }

    inline float time_allowed() const
    { return time_allowed_; }

    inline void time_allowed(float time_allowed)
    { time_allowed_ = time_allowed; }

    /** 
     * @brief Fixed return value
     * 
     * @return "session-init"
     */
    inline virtual const char *type() const
    { return "session-init"; }

    /**
     * get_xml method particularized for this message.
     * @param xml string where the xml string shall be stored
     * @see ServerMessage
     */
    virtual void get_xml(std::string &xml) const;

    /**
     * print method particularized for this message.
     * @param os Output stream where the messaged shall be printed to.
     */
    virtual void print(std::ostream &os) const;
};


/**
 * This kind of message is sent by the server each time a new round begins.
 * It contains the remaining number of this round, the number of remaining
 * rounds and the time that is left.
 */
class RoundInitMsg : public ServerMessage
{
  private:

    int round_num_, rounds_left_;
    float time_left_;

  public:

    typedef boost::shared_ptr<RoundInitMsg> Ptr;
    typedef boost::shared_ptr<const RoundInitMsg> ConstPtr;

    /**
     * Constructor & initializator
     * @param round_num The number of this round (defaults to 0)
     * @param rounds_left The number of remaining rounds (defaults to 0)
     * @param time_left The time that is left (defaults to a big value)
     * @return
     */
    RoundInitMsg(int round_num=0, int rounds_left=0, float time_left=9E6);

    // Just a bunch of getters and setters. Their usage should be self-explanatory,
    // so they are left undocumented.

    inline int round_num() const
    { return round_num_; }

    inline void round_num(int round_num)
    { round_num_ = round_num; }

    inline int rounds_left() const
    { return rounds_left_; }

    inline void rounds_left(int rounds_left)
    { rounds_left_ = rounds_left; }

    inline float time_left() const
    { return time_left_; }

    inline void time_left(float time_left)
    { time_left_ = time_left; }

    /**
     * @brief Fixed return value
     * 
     * @return "round-init"
     */
    inline virtual const char *type() const
    { return "round-init"; }

    /**
     * get_xml method particularized for this message.
     * @param xml string where the xml string shall be stored
     * @see ServerMessage
     */
    virtual void get_xml(std::string &xml) const;

    /**
     * print method particularized for this message.
     * @param os Output stream where the messaged shall be printed to.
     */
    virtual void print(std::ostream &os) const;

};


/**
 * This kind of message is sent by the server as a response to each of the
 * planner actions. It contains the current state's (what a state is depends
 * on the problem) fluents, the immediate reward, the turn, and the time left.
 */
class StateMsg : public ServerMessage
{
  public: // Declaration of types

    /**
     * This structure represents an observed fluent. An observed fluent
     * is composed by its name, its value (whether it is asserted or
     * negated and a list of arguments).
     *
     * @brief structure representation of an observed fluent
     */
    struct ObservedFluent
    {
      std::string name;
      std::vector<std::string> args;
      bool value;
    };

    typedef std::vector<ObservedFluent> ObservedFluents;

    typedef boost::shared_ptr<StateMsg> Ptr;
    typedef boost::shared_ptr<const StateMsg> ConstPtr;

  private:

    int turn_num_;
    float time_left_, immediate_reward_;
    ObservedFluents observed_fluents_;

    // auxiliary empty vector whose purpose is to set a default value for the
    // constructor (the type is a const reference so it cannot be a temp value)
    static const ObservedFluents NO_FLUENTS;

  public:

    /**
     * Constructor & initializator
     *
     * @param turn_num Initial value of the turn-num property (defaults to 1)
     * @param time_left Initial value of the time-left property
     * (defaults to high value)
     * @param immediate_reward Initial value of the immediate-reward property
     * (defaults to 0)
     * @param observed_fluents State fluents (defaults to empty vector)
     */
    StateMsg(int turn_num=1, float time_left=9E6, float immediate_reward=0,
             const ObservedFluents &observed_fluents=NO_FLUENTS);

    // Just a bunch of getters and setters. Their usage should be self-explanatory,
    // so they are left undocumented.

    inline int turn_num() const
    { return turn_num_; }

    inline void turn_num(int turn_num)
    { turn_num_ = turn_num; }

    inline float time_left() const
    { return time_left_; }

    inline void time_left(float time_left)
    { time_left_ = time_left; }

    inline float immediate_reward() const
    { return immediate_reward_; }

    inline void immediate_reward(float immediate_reward)
    { immediate_reward_ = immediate_reward; }

    inline const ObservedFluents &observed_fluents() const
    { return observed_fluents_; }

    inline void observed_fluents(const ObservedFluents &observed_fluents)
    { observed_fluents_ = observed_fluents; }

    /** 
     * @brief Fixed return value
     * 
     * @return "turn"
     */
    inline virtual const char *type() const
    { return "turn"; }

    /**
     * get_xml method particularized for this messages.
     * @param xml string where the xml string shall be stored
     * @see ServerMessage
     */
    virtual void get_xml(std::string &xml) const;

    /**
     * print method particularized for this message.
     * @param os Output stream where the messaged shall be printed to.
     */
    virtual void print(std::ostream &os) const;

};


/**
 * This kind of message is sent each time a round finishes. It basically
 * contains the name of the problem instance, the client name, the number
 * of the round that has just finished, the number of turns used in the previous
 * round, the reward for the round, the employed time, the time that is left for
 * the next round and the immediate reward of the last action.
 */
class EndRoundMsg : public ServerMessage
{
  private:

    std::string instance_name_, client_name_;
    int round_num_, turns_used_;
    float round_reward_, time_used_, time_left_, immediate_reward_;

  public:

    typedef boost::shared_ptr<EndRoundMsg> Ptr;
    typedef boost::shared_ptr<const EndRoundMsg> ConstPtr;

    /**
     * Constructor and initializator
     *
     * @param instance_name Name of the problem instance (defaults to empty string)
     * @param client_name Name of the client (defaults to empty string)
     * @param round_num Number of the round (defaults to 0)
     * @param turns_used Turns used in the round (defaults to 0)
     * @param round_reward Reward for that round (defaults to 0)
     * @param time_used Time employed (defaults to 0)
     * @param time_left Time left (defaults to high value)
     * @param immediate_reward Immediate reward from the last actions
     * @return
     */
    EndRoundMsg(const std::string &instance_name="", const std::string &client_name="",
                int round_num=0, int turns_used=0, float round_reward=0,
                float time_used=0, float time_left=9E6, float immediate_reward=0);

    // Just a bunch of getters and setters. Their usage should be self-explanatory,
    // so they are left undocumented.

    inline const std::string &instance_name() const
    { return instance_name_; }

    inline void instance_name(const std::string &instance_name)
    { instance_name_ = instance_name; }

    inline const std::string &client_name() const
    { return client_name_; }

    inline void client_name(const std::string &client_name)
    { client_name_ = client_name; }

    inline int round_num() const
    { return round_num_; }

    inline void round_num(int round_num)
    { round_num_ = round_num; }

    inline int turns_used() const
    { return turns_used_; }

    inline void turns_used(int turns_used)
    { turns_used_ = turns_used; }

    inline float round_reward() const
    { return round_reward_; }

    inline void round_reward(float round_reward)
    { round_reward_ = round_reward; }

    inline float time_used() const
    { return time_used_; }

    inline void time_used(float time_used)
    { time_used_ = time_used; }

    inline float time_left() const
    { return time_left_; }

    inline void time_left(float time_left)
    { time_left_ = time_left; }

    inline float immediate_reward() const
    { return immediate_reward_; }

    inline void immediate_reward(float immediate_reward)
    { immediate_reward_ = immediate_reward; }

    /** 
     * @brief Fixed return value
     * 
     * @return "round-end"
     */
    inline virtual const char *type() const
    { return "round-end"; }

    /**
     * get_xml method particularized for this messages.
     * @param xml string where the xml string shall be stored
     * @see ServerMessage
     */
    virtual void get_xml(std::string &xml) const;

    /**
     * print method particularized for this message.
     * @param os Output stream where the messaged shall be printed to.
     */
    virtual void print(std::ostream &os) const;
};


/**
 * Sent by the server at the end of the session. It contains the session's
 * identifier, the problem's name, the client's name, the total reward, the
 * employed time and the total number of rounds.
 */
class EndSessionMsg : public ServerMessage
{
  private:

    std::string session_id_, instance_name_, client_name_;
    float total_reward_, time_used_;
    int rounds_used_;

  public:

    typedef boost::shared_ptr<EndSessionMsg> Ptr;
    typedef boost::shared_ptr<const EndSessionMsg> ConstPtr;

    /**
     * Constructor and initializator
     * @param session_id Identifier of the session (defaults to "")
     * @param instance_name Name of the problem instance (defaults to "")
     * @param client_name Name of the client (defaults to "")
     * @param total_reward Total reward (defaults to 0)
     * @param time_used Total employed time (defaults to 0)
     * @param rounds_used Total number of rounds (defualts to 0)
     * @return
     */
    EndSessionMsg(const std::string &session_id="", const std::string &instance_name="",
                  const std::string &client_name="",float total_reward=0,
                  float time_used=0, int rounds_used=0);

    // Just a bunch of getters and setters. Their usage should be self-explanatory,
    // so they are left undocumented.

    inline const std::string &session_id() const
    { return session_id_; }

    inline void session_id(const std::string &session_id)
    { session_id_ = session_id; }

    inline const std::string &instance_name() const
    { return instance_name_; }

    inline void instance_name(const std::string &instance_name)
    { instance_name_ = instance_name; }

    inline const std::string &client_name() const
    { return client_name_; }

    inline void client_name(const std::string &client_name)
    { client_name_ = client_name; }

    inline float total_reward() const
    { return total_reward_; }

    inline void total_reward(float total_reward)
    { total_reward_ = total_reward; }

    inline float time_used() const
    { return time_used_; }

    inline void time_used(float time_used)
    { time_used_ = time_used; }

    inline int rounds_used() const
    { return rounds_used_; }

    inline void rounds_used(int rounds_used)
    { rounds_used_ = rounds_used; }

    /** 
     * @brief Fixed return value
     * 
     * @return "session-end"
     */
    inline virtual const char *type() const
    { return "session-end"; }

    /**
     * get_xml method particularized for this messages.
     * @param xml string where the xml string shall be stored
     * @see ServerMessage
     */
    virtual void get_xml(std::string &xml) const;

    /**
     * print method particularized for this message.
     * @param os Output stream where the messaged shall be printed to.
     */
    virtual void print(std::ostream &os) const;


};

/////////////////////
// CLIENT MESSAGES //
/////////////////////


/** 
 * @brief Abstract class that represents a Message from the client side.
 * Our server has to parse and process these messages.
 */
class ClientMessage : public Message
{
  public:

    typedef boost::shared_ptr<ClientMessage> Ptr;
    typedef boost::shared_ptr<const ClientMessage> ConstPtr;

    /** 
     * @brief Constant return value.
     * 
     * @return false
     */
    inline virtual bool server() const
    { return false; }

    virtual void print(std::ostream &os) const =0;

};


/**
 * @brief SessionRquest message. Tells the server the name of the client
 * and the problem that is going to be solved.
 */
class SessionRequestMsg : public ClientMessage
{
  private:

    std::string client_name_, problem_name_;

    /** 
     * @brief Make constructor private. The only one who should be able to
     * create objects from this class is Parser
     *
     * @see Parser
     */
    SessionRequestMsg()
    {}

  public:

    typedef boost::shared_ptr<SessionRequestMsg> Ptr;
    typedef boost::shared_ptr<const SessionRequestMsg> ConstPtr;

    /** 
     * @brief Getter of the client name.
     * 
     * @return Client name
     */
    inline const std::string &client_name() const
    { return client_name_; }

    /** 
     * @brief Getter of the problem name
     * 
     * @return Problem name
     */
    inline const std::string &problem_name() const
    { return problem_name_; }

    /** 
     * @brief Fixed return value
     * 
     * @return "session-request"
     */
    inline virtual const char *type() const
    { return "session-request"; }

    /** 
     * @brief Prints stream representation of this message to stream
     * 
     * @param os Output stream
     */
    virtual void print(std::ostream &os) const;

    friend class Parser;
};


/** 
 * @brief No big particularities about this message (it does not contain
 * more information aside from the Message type itself).
 */
class RoundRequestMsg : public ClientMessage
{
  private:
    /** 
     * @brief Make constructor private. The only one who should be able to
     * create objects from this class is Parser
     *
     * @see Parser
     */
    RoundRequestMsg()
    {}

  public:

    typedef boost::shared_ptr<RoundRequestMsg> Ptr;
    typedef boost::shared_ptr<const RoundRequestMsg> ConstPtr;

    /** 
     * @brief Fixed return value
     * 
     * @return "round-request"
     */
    inline virtual const char *type() const
    { return "round-request"; }

    /** 
     * @brief Prints a stream representation of this message to a given
     * output stream
     * 
     * @param os Output stream
     */
    inline virtual void print(std::ostream &os) const
    { os << "round-request\n"; }

    friend class Parser;
};


/** 
 * @brief Message of type actions. Contains a list of Action each of which
 * consists of a name, a list of arguments and a value.
 */
class ActionsMsg : public ClientMessage
{
  public: // type definitions

    typedef boost::shared_ptr<ActionsMsg> Ptr;
    typedef boost::shared_ptr<const ActionsMsg> ConstPtr;

    /**
     * @brief Groups the fields of a single action
     */
    struct Action
    {
      std::string name;
      bool value;
      std::vector<std::string> args;
    };
    typedef std::vector<Action> Actions;

  private: // private fields

    Actions actions_;


    /** 
     * @brief Make constructor private. The only one who should be able to
     * create objects from this class is Parser
     *
     * @see Parser
     */
    ActionsMsg()
    {}

  public: // public methods

    /** 
     * @brief Fixed return value
     * 
     * @return "actions"
     */
    inline virtual const char *type() const
    { return "actions"; }

    /** 
     * @brief List of actions
     * 
     * @return List of actions as a vector of Action structs.
     *
     * @see Action
     */
    inline const Actions &actions() const
    { return actions_; }

    /** 
     * @brief Prints a stream representation of this message to a given
     * output stream
     * 
     * @param os Output stream
     */
    virtual void print(std::ostream &os) const;

    friend class Parser;
};


/**
 * Parser class. It is employed to parse a XML string into a ClientMessage.
 */
class Parser
{
  private:

    std::string error_msg_;
    ClientMessage::ConstPtr msg_;

    /** 
     * @brief Gets the text of a given XMLEelement's child, or an empty
     * string if there is not such child.
     * 
     * @param root Pointer to a XMLElement
     * @param child The name of a root's child
     * 
     * @return Child's text
     */
    static const char *get_text(tinyxml2::XMLElement *root, const char *child);

    /** 
     * @brief Parses XMLElement as a session-request and creates a
     * SessionRequestMsg with the node's information.
     * 
     * @param root XMLElement to be parsed. Assumed to represent a
     * session-request
     */
    void parse_session_request(tinyxml2::XMLElement *root);

    /** 
     * @brief Parses XMLElement as a session-request and creates a
     * SessionRequestMsg with the node's information.
     * 
     * @param root Ignored. round-request messages do not contain any
     * information.
     */
    void parse_round_request(tinyxml2::XMLElement *root);

    /** 
     * @brief Parses XMLElement as a list of actions  and creates a
     * SessionRequestMsg with the node's information.
     * 
     * @param root XMLElement to be parsed. Assumed to represent an actions
     * message.
     */
    void parse_actions(tinyxml2::XMLElement *root);

  public:

    /** 
     * @brief 
     * 
     * @return 
     */
    inline bool success() const
    { return (bool) msg_; }

    /** 
     * @brief Parses a given string and, if the format is correct, stores
     * the result internally as a Message object.
     * 
     * @param xml 0-terminated string to be parsed
     * 
     * @return The same parser so several methods can be chained (e.g. the
     * bool operator in a if statement, or the message method just after
     * parsing)
     */
    Parser &parse(const char *xml);

    /** 
     * @brief This method is provided for convenience. It does the same
     * than the one that takes a C-string, but takes a C++ string as input
     * instead.
     * 
     * @param xml string to be parsed
     * 
     * @return Same as parse(const char*)
     */
    inline Parser &parse(const std::string &xml)
    { return parse(xml.c_str()); }

    /** 
     * @brief Same as success.
     * 
     * @return true if the last call to parse was successful
     *
     * @see success
     */
    inline operator bool() const
    { return success(); }

    /** 
     * @brief Returns the last parsed message (notice it can be null if
     * the last call to parse failed).
     * 
     * @return Parsed message.
     */
    inline ClientMessage::ConstPtr message() const
    { return msg_; }

    /** 
     * @brief Returns an explanation for the last error (or an empty string
     * if the last call to parse was successful)
     * 
     * @return error description as a string.
     */
    inline const std::string &error_str() const
    { return error_msg_; }

};


/** 
 * @brief Convenience method to print a Message to an output stream. Uses the
 * print method of the class Message
 * 
 * @param os Output stream
 * @param message Message to be printed
 * 
 * @return 
 */
std::ostream &operator<<(std::ostream &os, const Message &message);

}

#endif //ICAPS_SERVER_MESSAGES_H
