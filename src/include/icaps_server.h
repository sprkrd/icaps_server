#ifndef ICAPS_SERVER_H
#define ICAPS_SERVER_H

#include "messages.h"

namespace icaps_server
{

class Problem
{
  public:

    typedef boost::shared_ptr<Problem> Ptr;
    typedef boost::shared_ptr<const Problem> ConstPtr;

    virtual const char* instance_name() const=0;

    virtual StateMsg::ConstPtr state_msg() const=0;

    virtual bool done() const=0;

    virtual bool apply_actions(ActionsMsg::ConstPtr actions) =0;

};

class IcapsServer
{
  private:

    int port_, fd_, conn_;

    bool error_;
    std::string error_str_;

    Problem::Ptr problem_;

    bool check_condition(bool condition, const char* m1, const char* m2=0);

  public:

    /**
     *
     * @param port
     * @return
     */
    IcapsServer(Problem::Ptr problem, int port=2323);

    /**
     *
     * @return
     */
    inline int port() const
    { return port_; }

    /**
     *
     * @return
     */
    inline bool error() const
    { return error_; }

    /**
     *
     * @return
     */
    inline const std::string &error_str() const
    { return error_str_; }

    void run();

    /**
     *
     */
    ~IcapsServer();
};



}

#endif
