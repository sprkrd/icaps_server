#include "icaps_server.h"
#include <iostream>
#include <map>


using namespace icaps_server;

class ListSort : public Problem
{
  public:

    typedef std::string Object, Position;
    typedef std::map<Object,Position> State;

  private:

    State current_, goal_;
    int turn_num_;
    float cost_, immediate_reward_;

  public:

    ListSort() : turn_num_(1), cost_(0), immediate_reward_(0)
    {}

    inline State& current()
    { return current_; }

    inline State& goal()
    { return goal_; }

    virtual const char *instance_name() const
    {
      return "list-sort";
    }

    virtual StateMsg::ConstPtr state_msg() const
    {
      typedef State::const_iterator Iter;
      StateMsg::ObservedFluents fluents(current_.size());

      int idx = 0;
      for (Iter it=current_.begin(); it!=current_.end(); ++it)
      {
        fluents[idx].name = "position";
        fluents[idx].args.resize(2);
        fluents[idx].args[0] = it->first;
        fluents[idx].args[1] = it->second;
        fluents[idx].value = true;
        ++idx;
      }

      StateMsg::ConstPtr state(new StateMsg(turn_num_,9E6,immediate_reward_,fluents));
      return state;
    }

    virtual bool done() const
    {
      return current_.size()==goal_.size() and
          std::equal(current_.begin(), current_.end(), goal_.begin());
    }

    virtual bool apply_actions(ActionsMsg::ConstPtr actions)
    {
      for (int idx=0; idx < actions->actions().size(); ++idx)
      {
        const ActionsMsg::Action& action = actions->actions()[idx];
        std::swap(current_[action.args[0]], current_[action.args[1]]);
        cost_ += 1;
        immediate_reward_ = -1;
      }
      return true;
    }
};

int main(int argc, char *argv[])
{
  ListSort* ptr = new ListSort;
  ptr->current()["a"] = "2";
  ptr->current()["b"] = "1";
  ptr->goal()["a"] = "1";
  ptr->goal()["b"] = "2";
  Problem::Ptr problem(ptr);
  icaps_server::IcapsServer server(problem);

  if (server.error())
  {
    std::cerr << server.error_str() << '\n';
    return -1;
  }

  server.run();
  if (server.error())
  {
    std::cerr << server.error_str() << '\n';
    return -1;
  }
  else std::cout << "Server shut down correctly" << std::endl;
}
