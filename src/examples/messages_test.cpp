#include <iostream>
#include "messages.h"

const std::string ex1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
                         <session-request>\
                         <client-name>gourmand</client-name>\
                         <problem-name>list-sort</problem-name>\
                         </session-request>";

const std::string ex2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
                         <round-request/>";

const std::string ex3 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
                         <actions>\
                         <action>\
                         <action-name>swap</action-name>\
                         <action-arg>a</action-arg>\
                         <action-arg>b</action-arg>\
                         <action-value>true</action-value>\
                         </action>\
                         <action>\
                         <action-name>swap</action-name>\
                         <action-arg>b</action-arg>\
                         <action-arg>c</action-arg>\
                         </action>\
                         </actions>";

const std::string ex4 = "<another-thing><child>1</child></another-thing>";


int main(int /*argc*/, char** /*argv*/)
{
  icaps_server::Parser parser;

  // Test parsing a session-request msg
  if (parser.parse(ex1))
  {
    std::cout << *parser.message() << std::endl;
    // Notice that you can cast down to the SessionRequestMsg type
    // (done here for illustration purposes, although thanks to the
    // virtual print method it is not necessary.

  }
  else
    std::cerr << parser.error_str() << '\n';

  // Test parsing a round-request msg
  if (parser.parse(ex2))
    std::cout << *parser.message() << std::endl;
  else
    std::cerr << parser.error_str() << '\n';

  // Test parsing an actions msg
  if (parser.parse(ex3))
    std::cout << *parser.message() << std::endl;
  else
    std::cerr << parser.error_str() << '\n';

  // Test parsing an unknown type
  if (parser.parse(ex4))
    std::cout << *parser.message() << std::endl;
  else // this time it should not be a correct message
    std::cerr << parser.error_str() << '\n';

  // Test another session init
  icaps_server::ServerMessage::Ptr msg(new icaps_server::SessionInitMsg("xi11",1,100.0));
  std::cout << *msg << std::endl;
  std::cout << msg->get_xml() << std::endl;

  // Test round init
  msg.reset(new icaps_server::RoundInitMsg());
  std::cout << *msg << std::endl;
  std::cout << msg->get_xml() << std::endl;

  // Test state without observable fluents
  msg.reset(new icaps_server::StateMsg());
  std::cout << *msg << std::endl;
  std::cout << msg->get_xml() << std::endl;

  // Test state with observable fluents
  icaps_server::StateMsg::ObservedFluents fluents(2);
  fluents[0].name = "on";
  fluents[0].value = false;
  fluents[0].args.resize(2);
  fluents[0].args[0] = "a";
  fluents[0].args[1] = "b";
  fluents[1].name = "clear";
  fluents[1].value = true;
  fluents[1].args.resize(1);
  fluents[1].args[0] = "a";
  msg.reset(new icaps_server::StateMsg(0,100,0,fluents));
  std::cout << *msg << std::endl;
  std::cout << msg->get_xml() << std::endl;

  // Test end round
  msg.reset(new icaps_server::EndRoundMsg("instance","client"));
  std::cout << *msg << std::endl;
  std::cout << msg->get_xml() << std::endl;

  // Test end session
  msg.reset(new icaps_server::EndSessionMsg("id","instance","client"));
  std::cout << *msg;
  std::cout << msg->get_xml() << std::endl;
}
