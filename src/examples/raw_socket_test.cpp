//
// Created by asuarez on 10/11/16.
//
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <netinet/in.h>
#include <cstring>

void error(const char* m)
{
  perror(m);
  exit(-1);
}

int main(int argc, char* argv[])
{
  const uint16_t port = (in_port_t)atoi(argv[1]);
  const int yes = 0x1;
  int fd = socket(AF_INET, SOCK_STREAM, 0);
  setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int));
  printf("Creating socket descriptor...\n");
  if (fd < 0) error("socket");
  printf("Created.");
  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_port = htons(port);
  address.sin_addr.s_addr = INADDR_ANY;
  memset(address.sin_zero, 0, 7);

  printf("Binding to port_ %d...\n", port);
  if (bind(fd, (struct sockaddr*)&address, sizeof(sockaddr_in)) < 0) error("bind");
  printf("Bound.\n");

  printf("Start listening...\n");
  if (listen(fd,1) < 0) error("listen");
  printf("Listening on port_ %d\n", port);

  struct sockaddr client;

  int conn;
  socklen_t client_s;

  printf("Waiting connection...\n");
  conn = accept(fd, &client, &client_s);
  printf("Connection accepted.\n");

  char buffer[256];
  ssize_t nread, nwrite;

  printf("Reading from client...\n");
  while ((nread = read(conn,buffer,255)) > 0)
  {
    buffer[nread] = '\0';
    printf("Received: %s", buffer);
    // Echo back
    nwrite = write(conn,buffer,(size_t)nread);
    if (nwrite < 0)
    {
      close(conn);
      error("write");
    }
  }
  close(conn);
  close(fd);
  if (nread < 0) error("read");
}

