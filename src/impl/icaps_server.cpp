#include "icaps_server.h"
#include <cerrno>
#include <sstream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>

namespace icaps_server
{



bool IcapsServer::check_condition(bool condition, const char *m1, const char *m2)
{
  if (not condition)
  {
    error_ = true;
    if (m2==0) m2 = strerror(errno);
    std::ostringstream oss;
    oss << m1 << ": " << m2;
    error_str_ = oss.str();
  }
  return condition;
}

IcapsServer::IcapsServer(Problem::Ptr problem, int port) : port_(port), fd_(-1),
                                                           conn_(-1), error_(false),
                                                           error_str_(""), problem_(problem)
{
  int status, yes = 0x1;

  // Create a socket and associate a file descriptor to it.
  fd_ = socket(AF_INET, SOCK_STREAM, 0);
  if (not check_condition(fd_>=0, "socket")) return;

  // Set the SO_REUSEADDR property to true
  status = setsockopt(fd_, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
  if (not check_condition(status==0, "setsockopt")) return;

  // Fill sockaddr structure and bind socket to port
  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_port = htons((uint16_t)port);
  address.sin_addr.s_addr = INADDR_ANY;
  memset(address.sin_zero, 0, 8);
  status = bind(fd_, (struct sockaddr*)&address, sizeof(sockaddr_in));
  if (not check_condition(status==0, "bind")) return;

  // Start listening on port
  status = listen(fd_, 1);
  check_condition(status==0, "listen");
}

void IcapsServer::run()
{
  Parser parser;
  char buffer[1024];
  ssize_t nread;
  // First accept client
  struct sockaddr_in client;
  socklen_t client_s;
  conn_ = accept(fd_, (struct sockaddr*)&client, &client_s);
  while ((nread = read(conn_,buffer,1023)) > 0)
  {
    buffer[nread] = '\0'; // Important! Place null terminator (read does not do it)
    if (parser.parse(buffer))
    {
      ServerMessage::ConstPtr reply;
      Message::ConstPtr message = parser.message();
      std::cout << *message;
      if (ActionsMsg::ConstPtr actions =
          boost::dynamic_pointer_cast<const ActionsMsg>(message))
      {
        problem_->apply_actions(actions);
        if (problem_->done())
          reply.reset(new EndRoundMsg(problem_->instance_name()));
        else reply = problem_->state_msg();
      }
      else if (SessionRequestMsg::ConstPtr session_req =
          boost::dynamic_pointer_cast<const SessionRequestMsg>(message))
      {
        reply.reset(new SessionInitMsg("session", 1));
      }
      else
      {
        RoundRequestMsg::ConstPtr round_req =
            boost::dynamic_pointer_cast<const RoundRequestMsg>(message);
        reply.reset(new RoundInitMsg(1,0));
      }
      std::string xml;
      reply->get_xml(xml);
      write(conn_, xml.c_str(), xml.length());
      if (problem_->done())
      {
        reply.reset(new EndSessionMsg("session", problem_->instance_name()));
        reply->get_xml(xml);
        write(conn_, xml.c_str(), xml.length());
        break;
      }
    }
    else std::cerr << parser.error_str() << '\n';
  }
  //check_condition(nread==0,"read");
  close(conn_);
  conn_ = -1;
}

IcapsServer::~IcapsServer()
{
  // Close client connection and server socket
  if (conn_ > 0) close(conn_);
  if (fd_ > 0) close(fd_);
}

}