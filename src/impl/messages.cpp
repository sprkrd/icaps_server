/** 
 * @file messages.cpp
 * @brief Implementation of the methods and classes declared in messages.h
 * @author Alejandro Suarez
 * @version 0.1
 * @date 2016-11-07
 */
#include "messages.h"
#include <sstream>

namespace icaps_server
{

///////////////////
//SERVER MESSAGES//
///////////////////


const std::string &ServerMessage::get_xml()
{
  get_xml(xml_tmp_);
  return xml_tmp_;
}

// SESSION INIT

SessionInitMsg::SessionInitMsg(const std::string &id, int n_rounds, float time_allowed)
    : session_id_(id), n_rounds_(n_rounds), time_allowed_(time_allowed)
{}

void SessionInitMsg::print(std::ostream &os) const
{
  os << "session-init:\n";
  os << "  session-id: " << session_id_ << '\n';
  os << "  n-rounds: " << n_rounds_ << '\n';
  os << "  time-allowed: " << time_allowed_ << '\n';
}

void SessionInitMsg::get_xml(std::string &xml) const
{
  std::ostringstream oss;
  oss << "<session-init>"
      << "<session-id>" << session_id_ << "</session-id>"
      << "<num-rounds>" << n_rounds_ << "</num-rounds>"
      << "<time-allowed>" << time_allowed_ << "</time-allowed>"
      << "</session-init>";
  xml = oss.str();
}

// ROUND INIT

RoundInitMsg::RoundInitMsg(int round_num, int rounds_left, float time_left)
    : round_num_(round_num), rounds_left_(rounds_left), time_left_(time_left)
{}

void RoundInitMsg::get_xml(std::string &xml) const
{
  std::ostringstream oss;
  oss << "<round-init>"
      << "<round-num>" << round_num_ << "</round-num>"
      << "<time-left>" << time_left_ << "</time-left>"
      << "<round-left>" << rounds_left_ << "</round-left>"
      << "</round-init>";
  xml = oss.str();
}

void RoundInitMsg::print(std::ostream &os) const
{
  os << "round-init:\n"
     << "  round-num: " << round_num_ << '\n'
     << "  time-left: " << time_left_ << '\n'
     << "  round-left: " << rounds_left_ << '\n';
}

// STATE

const StateMsg::ObservedFluents StateMsg::NO_FLUENTS(0);

StateMsg::StateMsg(int turn_num, float time_left, float immediate_reward,
                   const StateMsg::ObservedFluents &observed_fluents)
    : turn_num_(turn_num), time_left_(time_left), immediate_reward_(immediate_reward),
      observed_fluents_(observed_fluents)
{}

void StateMsg::print(std::ostream &os) const
{
  os << "turn:\n"
     << "  time-left: " << time_left_ << '\n'
     << "  immediate-reward: " << immediate_reward_ << '\n'
     << "  observed-fluents:\n";
  for (size_t idx = 0; idx < observed_fluents_.size(); ++idx)
  {
    const ObservedFluent& fluent = observed_fluents_[idx];
    os << "    ";
    if (not fluent.value) os << "¬";
    os << fluent.name << '(';
    if (fluent.args.size() > 0) os << fluent.args[0];
    for (size_t jdx = 1; jdx < fluent.args.size(); ++jdx)
      os << ',' << fluent.args[jdx];
    os << ")\n";
  }
  if (observed_fluents_.empty()) os << "    There are not observable fluents";
}

void StateMsg::get_xml(std::string &xml) const
{
  std::ostringstream oss;
  oss << "<turn>"
      << "<turn-num>" << turn_num_ << "</turn-num>"
      << "<time-left>" << time_left_ << "</time-left>"
      << "<immediate-reward>" << immediate_reward_ << "</immediate-reward>";
  for (size_t idx = 0; idx < observed_fluents_.size(); ++idx)
  {
    const ObservedFluent& fluent = observed_fluents_[idx];
    oss << "<observed-fluent>"
        << "<fluent-name>" << fluent.name << "</fluent-name>"
        << "<fluent-value>" << (fluent.value? "true" : "false") << "</fluent-value>";
    for (size_t jdx = 0; jdx < fluent.args.size(); ++jdx)
    {
      oss << "<fluent-arg>" << fluent.args[jdx] << "</fluent-arg>";
    }
    oss << "</observed-fluent>";
  }
  if (observed_fluents_.empty()) oss << "<no-observed-fluents/>";
  oss << "</turn>";
  xml = oss.str();
}

// END ROUND

EndRoundMsg::EndRoundMsg(const std::string &instance_name, const std::string &client_name,
                         int round_num, int turns_used, float round_reward,
                         float time_used, float time_left, float immediate_reward)
    : instance_name_(instance_name), client_name_(client_name), round_num_(round_num),
      turns_used_(turns_used), round_reward_(round_reward), time_used_(time_used),
      time_left_(time_left), immediate_reward_(immediate_reward)
{}

void EndRoundMsg::print(std::ostream &os) const
{
  os << "end-round:\n"
     << "  instance_name: " << instance_name_ << '\n'
     << "  client-name: " << client_name_ << '\n'
     << "  round-num: " << round_num_ << '\n'
     << "  round-reward: " << round_reward_ << '\n'
     << "  turns-used: " << turns_used_ << '\n'
     << "  time-used: " << time_used_ << '\n'
     << "  time-left: " << time_left_ << '\n'
     << "  immediate-reward: " << immediate_reward_ << '\n';
}

void EndRoundMsg::get_xml(std::string &xml) const
{
  std::ostringstream oss;
  oss << "<end-round>"
     << "<instance-name>" << instance_name_ << "</instance-name>"
     << "<client-name>" << client_name_ << "</client-name>"
     << "<round-num>" << round_num_ << "</round-num>"
     << "<round-reward>" << round_reward_ << "</round-reward>"
     << "<turns-used>" << turns_used_ << "</turns-used>"
     << "<time-used>" << time_used_ << "</time-used>"
     << "<time-left>" << time_left_ << "</time-left>"
     << "<immediate-reward>" << immediate_reward_ << "</immediate-reward>"
     << "</end-round>";
  xml = oss.str();
}

// END SESSION

EndSessionMsg::EndSessionMsg(const std::string &session_id,
                             const std::string &instance_name,
                             const std::string &client_name, float total_reward,
                             float time_used, int rounds_used)
    : session_id_(session_id), instance_name_(instance_name), client_name_(client_name),
      total_reward_(total_reward), time_used_(time_used), rounds_used_(rounds_used)
{}

void EndSessionMsg::print(std::ostream &os) const
{
  os << "session-end:\n"
     << "  session-id: " << session_id_ << '\n'
     << "  instance-name: " << instance_name_ << '\n'
     << "  client-name:  " << client_name_ << '\n'
     << "  total-reward: " << total_reward_ << '\n'
     << "  time-used: " << time_used_ << '\n'
     << "  rounds-used: " << rounds_used_ << '\n';
}

void EndSessionMsg::get_xml(std::string &xml) const
{
  std::ostringstream oss;
  oss << "<session-end>"
      << "<session-id>" << session_id_ << "</session-id>"
      << "<instance-name>" << instance_name_ << "</instance-name>"
      << "<client-name>" << client_name_ << "</client-name>"
      << "<total-reward>" << total_reward_ << "</total-reward>"
      << "<time-used>" << time_used_ << "</time-used>"
      << "<rounds-used>" << rounds_used_ << "</rounds-used>"
      << "</session-end>";
  xml = oss.str();
}

///////////////////
//CLIENT MESSAGES//
///////////////////

// SESSION REQUEST

void SessionRequestMsg::print(std::ostream &os) const
{
  os << "session-request:\n"
     << "  client-name: " << client_name_ << '\n'
     << "  problem-name: " << problem_name_ << '\n';
}

// ACTIONS

void ActionsMsg::print(std::ostream &os) const
{
  os << "actions:\n";
  // Print all the actions
  for (size_t idx = 0; idx < actions_.size(); ++idx)
  {
    os << "  " << actions_[idx].name << '(';
    if (actions_[idx].args.size() > 0)
    {
      os << actions_[idx].args[0];
    }
    // Print all the arguments
    for (size_t jdx = 1; jdx < actions_[idx].args.size(); ++jdx)
    {
      os << ',' << actions_[idx].args[jdx];
    }
    os << "), value=" << (actions_[idx].value ? "true" : "false") << '\n';
  }
}

//////////
//PARSER//
//////////

const char *Parser::get_text(tinyxml2::XMLElement *root, const char *child)
{
  tinyxml2::XMLElement *child_node = root->FirstChildElement(child);
  return child_node ? child_node->GetText() : "";
}

void Parser::parse_session_request(tinyxml2::XMLElement *root)
{
  SessionRequestMsg *session_msg = new SessionRequestMsg;

  session_msg->client_name_ = std::string(get_text(root, "client-name"));
  session_msg->problem_name_ = std::string(get_text(root, "problem-name"));

  msg_.reset(session_msg);
}

void Parser::parse_round_request(tinyxml2::XMLElement * /* root */)
{
  // just create a new RoundRequestMsg
  msg_.reset(new RoundRequestMsg);
}

void Parser::parse_actions(tinyxml2::XMLElement *root)
{
  ActionsMsg *actions_msg = new ActionsMsg;

  // read all the actions
  tinyxml2::XMLElement *action = root->FirstChildElement("action");
  while (action)
  {
    ActionsMsg::Action act;
    act.name = std::string(get_text(action, "action-name"));
    act.value = get_text(action, "action-value") == std::string("true");
    // read all the arguments for this actions
    tinyxml2::XMLElement *action_arg = action->FirstChildElement("action-arg");
    while (action_arg)
    {
      act.args.push_back(action_arg->GetText());
      action_arg = action_arg->NextSiblingElement("action-arg");
    }
    actions_msg->actions_.push_back(act);

    action = action->NextSiblingElement("action");
  }

  msg_.reset(actions_msg);
}

Parser &Parser::parse(const char *xml)
{
  // cleanup
  msg_.reset();
  error_msg_.clear();

  tinyxml2::XMLDocument doc;
  doc.Parse(xml);

  if (doc.Error())
  {
    std::ostringstream oss;
    oss << "XMLDocument error id=" << doc.ErrorID() <<
        " str1=" << doc.GetErrorStr1() << " str2=" << doc.GetErrorStr2();
    error_msg_ = oss.str();
  }
  else
  {
    tinyxml2::XMLElement *root = doc.RootElement();
    std::string type(root ? root->Name() : "empty");
    if (type == "session-request") parse_session_request(root);
    else if (type == "round-request") parse_round_request(root);
    else if (type == "actions") parse_actions(root);
    else error_msg_ = "unknown type: " + type;
  }

  return *this;
}

/////////////////
//OTHER METHODS//
/////////////////


std::ostream &operator<<(std::ostream &os, const Message &message)
{
  // we just call the print method of the message
  message.print(os);
  return os;
}

}
