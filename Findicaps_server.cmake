#edit the following line to add the librarie's header files
FIND_PATH(icaps_server_INCLUDE_DIR icaps_server.h /usr/include/iridrivers/icaps_server /usr/local/include/iridrivers/icaps_server)

FIND_LIBRARY(icaps_server_LIBRARY
    NAMES icaps_server
    PATHS /usr/lib /usr/local/lib /usr/local/lib/iridrivers/icaps_server) 

IF (icaps_server_INCLUDE_DIR AND icaps_server_LIBRARY)
   SET(icaps_server_FOUND TRUE)
ENDIF (icaps_server_INCLUDE_DIR AND icaps_server_LIBRARY)

IF (icaps_server_FOUND)
   IF (NOT icaps_server_FIND_QUIETLY)
      MESSAGE(STATUS "Found icaps_server: ${icaps_server_LIBRARY}")
   ENDIF (NOT icaps_server_FIND_QUIETLY)
ELSE (icaps_server_FOUND)
   IF (icaps_server_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find icaps_server")
   ENDIF (icaps_server_FIND_REQUIRED)
ENDIF (icaps_server_FOUND)

